import Foundation

class Queue  {
    private var queue:[Element]=[]
    public func addElem(elem:Element){
       queue.append(elem)
    }
    public func getFirstElem ()->Element? {
        return queue.firstElem
    }

    public func deleteFirstElem(){
        if queue.firstElem==nil {
            return
        }
        else {
            queue.remove(at:0)
        }
    }
    public func printQueue (){
        print(queue)
    }
}

var queue=Queue()

queue.addElem(elem:1)
queue.addElem(elem:2)
queue.addElem(elem:3)

queue.printQueue()
queue.deleteFirstElem()
queue.printQueue()